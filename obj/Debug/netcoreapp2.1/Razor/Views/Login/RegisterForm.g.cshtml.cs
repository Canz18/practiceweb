#pragma checksum "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6f97648674a71c0e72e82239404f85ebd2fe4524"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Login_RegisterForm), @"mvc.1.0.view", @"/Views/Login/RegisterForm.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Login/RegisterForm.cshtml", typeof(AspNetCore.Views_Login_RegisterForm))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\PC\Documents\practice\practiceWeb\Views\_ViewImports.cshtml"
using practiceWeb;

#line default
#line hidden
#line 2 "C:\Users\PC\Documents\practice\practiceWeb\Views\_ViewImports.cshtml"
using practiceWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6f97648674a71c0e72e82239404f85ebd2fe4524", @"/Views/Login/RegisterForm.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"820213eb054236d6ff1d5e77748656f71a53f16a", @"/Views/_ViewImports.cshtml")]
    public class Views_Login_RegisterForm : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/Register.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
  
    ViewData["Title"] = "RegisterForm";
    Layout = "~/Views/Shared/_LayoutLogin.cshtml";

#line default
#line hidden
            BeginContext(102, 183, true);
            WriteLiteral("\r\n<div class=\"container\">\r\n    <div class=\"card card-register mx-auto mt-5\">\r\n        <div class=\"card-header\">Register an Account</div>\r\n        <div class=\"card-body\">\r\n            ");
            EndContext();
            BeginContext(285, 4025, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9cb2a0b25eac4e4fb4311e45e8e56be6", async() => {
                BeginContext(291, 1421, true);
                WriteLiteral(@"
                <div class=""form-group"">
                    <div class=""form-row"">
                        <div class=""col-md-4"">
                            <div class=""form-label-group"">
                                <input type=""text"" id=""firstName"" class=""form-control getDataCls"" name=""FirstName"" placeholder=""First name"" required=""required"" autofocus=""autofocus"">
                                <label for=""firstName"">First name</label>
                            </div>
                        </div>
                        <div class=""col-md-4"">
                            <div class=""form-label-group"">
                                <input type=""text"" id=""lastName"" class=""form-control getDataCls"" name=""LastName"" placeholder=""Last name"" required=""required"">
                                <label for=""lastName"">Last name</label>
                            </div>
                        </div>
                        <div class=""col-md-4"">
                            <div class=""form-");
                WriteLiteral(@"label-group"">
                                <input type=""text"" id=""middleName"" name=""MiddleName"" class=""form-control getDataCls"" placeholder=""Middle name"">
                                <label for=""lastName"">Middle name</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""form-group"">
");
                EndContext();
#line 35 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
                     if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                    {

#line default
#line hidden
                BeginContext(1818, 963, true);
                WriteLiteral(@"                        <div class=""form-row"">
                            <div class=""col-md-8"">
                                <div class=""form-label-group"">
                                    <input type=""email"" id=""inputEmail"" class=""form-control getDataCls"" name=""username"" placeholder=""Email address"" required=""required"">
                                    <label for=""inputEmail"">Email address</label>
                                </div>
                            </div>
                            <div class=""col-md-4"">
                                <div class=""form-label-group"">
                                    <input type=""text"" id=""userroles"" class=""form-control getDataCls"" value=""Admin"" name=""userRole"" placeholder=""User Role"" readonly>
                                    <label for=""inputEmail"">User Role</label>
                                </div>
                            </div>
                        </div>
");
                EndContext();
#line 51 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
                    }
                    else
                    {

#line default
#line hidden
                BeginContext(2853, 325, true);
                WriteLiteral(@"                        <div class=""form-label-group"">
                            <input type=""email"" id=""inputEmail"" class=""form-control getDataCls"" name=""username"" placeholder=""Email address"" required=""required"">
                            <label for=""inputEmail"">Email address</label>
                        </div>
");
                EndContext();
#line 58 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
                    }

#line default
#line hidden
                BeginContext(3201, 1102, true);
                WriteLiteral(@"                </div>
                <div class=""form-group"">
                    <div class=""form-row"">
                        <div class=""col-md-6"">
                            <div class=""form-label-group"">
                                <input type=""password"" id=""inputPassword"" name=""password"" class=""form-control getDataCls"" placeholder=""Password"" required=""required"">
                                <label for=""inputPassword"">Password</label>
                            </div>
                        </div>
                        <div class=""col-md-6"">
                            <div class=""form-label-group"">
                                <input type=""password"" id=""confirmPassword"" class=""form-control"" placeholder=""Confirm password"" required=""required"">
                                <label for=""confirmPassword"">Confirm password</label>
                            </div>
                        </div>
                    </div>
                </div>
                <button class");
                WriteLiteral("=\"btn btn-primary btn-block\" id=\"btn_register\">Register</button>\r\n            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4310, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 78 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
             if (!User.Identity.IsAuthenticated && !User.IsInRole("Admin"))
            {

#line default
#line hidden
            BeginContext(4404, 92, true);
            WriteLiteral("                <div class=\"text-center\">\r\n                    <a class=\"d-block small mt-3\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 4496, "\"", 4531, 1);
#line 81 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
WriteAttributeValue("", 4503, Url.Action("Index","Login"), 4503, 28, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(4532, 136, true);
            WriteLiteral(">Login Page</a>\r\n                    <a class=\"d-block small\" href=\"forgot-password.html\">Forgot Password?</a>\r\n                </div>\r\n");
            EndContext();
#line 84 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
            }
            else
            {

#line default
#line hidden
            BeginContext(4716, 92, true);
            WriteLiteral("                <div class=\"text-center\">\r\n                    <a class=\"d-block small mt-3\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 4808, "\"", 4842, 1);
#line 88 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
WriteAttributeValue("", 4815, Url.Action("Index","Main"), 4815, 27, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(4843, 45, true);
            WriteLiteral(">Dashboard Page</a>\r\n                </div>\r\n");
            EndContext();
#line 90 "C:\Users\PC\Documents\practice\practiceWeb\Views\Login\RegisterForm.cshtml"
            }

#line default
#line hidden
            BeginContext(4903, 36, true);
            WriteLiteral("        </div>\r\n    </div>\r\n</div>\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(4957, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4963, 40, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d309dfeb5f2b4a10a0ccfb77aa74c459", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(5003, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
