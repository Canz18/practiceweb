﻿$(document).on('click', '#btn_register', function () {
    var arrData = {};
    $('input.getDataCls').each(function () {
        var name = $(this).attr('name');
        var val = $(this).val();
        arrData[name] = val;
    });

    $.ajax({
        url: "RegisterUser",
        type: 'POST',
        data: {
            data: JSON.stringify(arrData)
        },
        success: function (result) {
            if (result.respcode == 1) {
                //var xOrigin = location.origin;
                //location.href = xOrigin + "/MainPage";
                alert("success");
            } else {
                alert(result.message);
            }
        },
        error: function (errorResult) {
            //You can handle the error gracefully here and allow your user to 
            //know what is going on.
        }
    });
});