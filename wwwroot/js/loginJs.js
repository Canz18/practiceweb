﻿$(document).on('click', '#submit_btn', function () {
    var email = $('#inputEmail').val();
    var password = $('#inputPassword').val();
    
    $.ajax({
        url: "Login/Auth",
        type: 'POST',
        data: {
            username: email,
            password: password
        },
        success: function (result) {
            if (result.respcode == 1) {
                var xOrigin = location.origin;
                location.href = xOrigin + "/MainPage";
            }
        },
        error: function (errorResult) {
            //You can handle the error gracefully here and allow your user to 
            //know what is going on.
            console.log(ErrorEvent.toString());
        }
    });
});

function validateEmail($email) {
    var str = {};
    if ($email != '') {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (emailReg.test($email) == false) {
            str.valid = false;
            str.message = "Wrong email format";
        } else {
            str.valid = true;
            str.message = "Success";
        }
    } else {
        str.valid = false;
        str.message = "Please fill in email address";
    }
    return str;
}