﻿using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.GeneralRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.LoginRepo
{
    public class LoginRepository: Repository<TbLogin> , ILoginRepository
    {
        private PracticeDBContext PracticeDBContext { get { return _context as PracticeDBContext; } }
        public LoginRepository(PracticeDBContext _db)
            : base(_db)
        {
            
        }

        public async Task<TbLogin> GetLoginAsyn(string username, string password)
        {
            return await Task.Run(() => PracticeDBContext
                                            .TbLogin
                                            .Where(x => x.Username == username && x.Password == password)
                                            .FirstOrDefault());
        }
    }
}
