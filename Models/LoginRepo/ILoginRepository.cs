﻿using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.GeneralRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.LoginRepo
{
    public interface ILoginRepository : IRepository<TbLogin>
    {
        Task<TbLogin> GetLoginAsyn(string username, string password);
        
    }
}
