﻿using Microsoft.EntityFrameworkCore;
using practiceWeb.Models.Dbcontext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace practiceWeb.Models.GeneralRepository
{
    public class Repository<GenericT> : IRepository<GenericT> where GenericT : class 
    {
        protected readonly PracticeDBContext _context;
        public Repository(PracticeDBContext context)
        {
            _context = context;
        }
        public IEnumerable<GenericT> Find(Expression<Func<GenericT, bool>> predicate)
        {
            return _context.Set<GenericT>().Where(predicate);
        }

        public IEnumerable<GenericT> GetAll()
        {
            return _context.Set<GenericT>().ToList();
        }

        public void Insert(GenericT param)
        {
            _context.Set<GenericT>().Add(param);
        }

        public void InsertBulk(IEnumerable<GenericT> param)
        {
            _context.Set<GenericT>().AddRange(param);
        }
        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
