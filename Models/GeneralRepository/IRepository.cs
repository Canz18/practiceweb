﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace practiceWeb.Models.GeneralRepository
{
    public interface IRepository<GenericT> : IDisposable
    {
        IEnumerable<GenericT> Find(Expression<Func<GenericT, bool>> predicate);
        IEnumerable<GenericT> GetAll();

        void Insert(GenericT param);
        void InsertBulk(IEnumerable<GenericT> param);

        int Complete();
    }
}
