﻿using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.LoginRepo;
using practiceWeb.Models.Response;
using practiceWeb.Models.UoW;
using practiceWeb.Models.UserInfoRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace practiceWeb.Models.LoginService
{
    public class LogServ : ILoginServ
    {
        //private static IUnitOfWork unitOfWork;
        private readonly ILoginRepository _login;
        private readonly IUserInfoRepository _userinfo;
        private static PracticeDBContext _context;
        public LogServ(PracticeDBContext context)
        {
            _context = context;
            _login = new LoginRepository(_context);
            _userinfo = new UserInfoRepository(_context);
        }
        public async Task<LoginResponse> getLoginInfo(string username, string password)
        {
            try
            {
                var y = await _login.GetLoginAsyn(username, password);
                var j = await _userinfo.GetLoginInfo(y.Id);

                return (y != null) ? new LoginResponse { respcode = 1, _data = y, _userdata = j } : new LoginResponse { respcode = 0, message = "Username and Password did not much" };
            }
            catch (Exception ex)
            {
                return new LoginResponse { respcode = 0, message = ex.Message };
            }
        }

        public async Task<generalResponse> AddUser(TbLogin logininfo, TbUserInfo userinfo)
        {
            try
            {
                userinfo.Login = logininfo;
                _userinfo.Insert(userinfo);
                await _context.SaveChangesAsync();
                return new generalResponse { respcode = 1, message = "success" };
            }
            catch (Exception ex)
            {
                return new generalResponse { respcode = 0, message = ex.Message };
            }
        }
    }
}
