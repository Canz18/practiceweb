﻿using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.LoginService
{
    public interface ILoginServ
    {
        Task<LoginResponse> getLoginInfo(string username, string password);
        Task<generalResponse> AddUser(TbLogin logininfo, TbUserInfo userinfo);
    }
}
