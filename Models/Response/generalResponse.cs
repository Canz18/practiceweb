﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.Response
{
    public class generalResponse
    {
        public int respcode { get; set; }
        public string message { get; set; }
    }
}
