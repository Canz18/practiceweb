﻿using practiceWeb.Models.Dbcontext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.Response
{
    public class LoginResponse : generalResponse
    {
        public TbUserInfo _userdata { get; set; }
        public TbLogin _data { get; set; }
    }
}
