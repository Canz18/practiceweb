﻿using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.GeneralRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.UserInfoRepo
{
    public interface IUserInfoRepository : IRepository<TbUserInfo>
    {
        IEnumerable<TbUserInfo> GetAllUser(int pageIndex, int pageSize);
        Task<TbUserInfo> GetLoginInfo(int id);
    }
}
