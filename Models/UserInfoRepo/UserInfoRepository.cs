﻿using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.GeneralRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.UserInfoRepo
{
    public class UserInfoRepository : Repository<TbUserInfo>, IUserInfoRepository
    {
        private  PracticeDBContext PracticeDBContext { get { return _context as PracticeDBContext; } }
        public UserInfoRepository(PracticeDBContext context)
            : base(context)
        {
        }
        
        public IEnumerable<TbUserInfo> GetAllUser(int pageIndex, int pageSize)
        {
            return PracticeDBContext.TbUserInfo
                 .Skip((pageIndex - 1) * pageSize)
                 .Take(pageSize)
                 .ToList();
        }

        public async Task<TbUserInfo> GetLoginInfo(int id)
        {
            return await Task.Run(() => PracticeDBContext.TbUserInfo.Where(item => item.LoginId == id).FirstOrDefault());
        }
    }
}
