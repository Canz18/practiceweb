﻿using practiceWeb.Models.LoginRepo;
using practiceWeb.Models.LoginService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace practiceWeb.Models.UoW
{
    public interface IUnitOfWork
    {
        ILoginServ _login { get; }
    }
}
