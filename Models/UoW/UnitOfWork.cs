﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.LoginRepo;
using practiceWeb.Models.LoginService;

namespace practiceWeb.Models.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        public ILoginServ _login { get; private set; }
        private static PracticeDBContext _context;
        public UnitOfWork(PracticeDBContext context)
        {
            _context = context;
            _login = new LogServ(_context);
        }
        
    }
}
