﻿using System;
using System.Collections.Generic;

namespace practiceWeb.Models.Dbcontext
{
    public partial class TbLogin
    {
        public TbLogin()
        {
            TbUserInfo = new HashSet<TbUserInfo>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserRole { get; set; }

        public ICollection<TbUserInfo> TbUserInfo { get; set; }
    }
}
