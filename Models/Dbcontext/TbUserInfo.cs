﻿using System;
using System.Collections.Generic;

namespace practiceWeb.Models.Dbcontext
{
    public partial class TbUserInfo
    {
        public int Id { get; set; }
        public int LoginId { get; set; }
        public string Firstname { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int PermissionId { get; set; }

        public TbLogin Login { get; set; }
    }
}
