﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace practiceWeb.Controllers
{
    [Authorize]
    public class MainController : Controller
    {
        [Route("MainPage")]
        //[Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            return View();
        }
        
        [Authorize(Roles = "User")]
        public IActionResult UserStartPage()
        {
            return View();
        }
    }
}