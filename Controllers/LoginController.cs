﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.UoW;
using Newtonsoft.Json;

namespace practiceWeb.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUnitOfWork _unitOfwork; 
        public LoginController(IUnitOfWork unitOfWork)
        {
            _unitOfwork = unitOfWork;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Route("Login/Auth")]
        public async Task<JsonResult> LoginAction(string username, string password)
        {
            var x = await _unitOfwork._login.getLoginInfo(username, password);
            if (x.respcode == 1)
            {
                var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, x._userdata.LastName),
                    new Claim(ClaimTypes.Email, username),
                    new Claim(ClaimTypes.Role, x._data.UserRole),
                    new Claim(ClaimTypes.Sid, "123")
                }, CookieAuthenticationDefaults.AuthenticationScheme
                 , ClaimTypes.Name, ClaimTypes.Role);



                var principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    principal);
            }
            return Json(x);
        }
        [Route("RegisterForm")]
        public IActionResult Register()
        {
            return View("~/Views/Login/RegisterForm.cshtml");
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction(nameof(Index));
        }
        [Route("RegisterUser")]
        public async Task<JsonResult> AddUserMethod(string data)
        {
            var loginInfo = (TbLogin)JsonConvert.DeserializeObject(data, typeof(TbLogin));
            var userInfo = (TbUserInfo)JsonConvert.DeserializeObject(data, typeof(TbUserInfo));
            loginInfo.UserRole = (string.IsNullOrEmpty(loginInfo.UserRole)) ? "User" : loginInfo.UserRole;
            userInfo.PermissionId = (loginInfo.UserRole == "Admin") ? 1 : 2;

            var x = await _unitOfwork._login.AddUser(loginInfo, userInfo);
            return Json(x);
        }
    }
}