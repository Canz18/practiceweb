﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using practiceWeb.Models;
using practiceWeb.Models.Dbcontext;
using practiceWeb.Models.GeneralRepository;
using practiceWeb.Models.UoW;

namespace practiceWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _UoW;
        public HomeController(IUnitOfWork unitOfWork)
        {
            _UoW = unitOfWork;
        }
        public IActionResult Index()
        {

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
